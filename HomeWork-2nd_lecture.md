homework is next:

Please implement next functions:
bool equals(double, double, unit8_t precision) // return true if two given numbers are equal. use precision co control the comparison
void setBit(int& src, uint8_t bit) // take a integer and set specific bit to 0 or 1
void revertBit(int& src, uint8_t bit) // take an integer and revert specified bit in it
bool addVector(const int* src1, const int* src2, size, int* dts, std::size_t size); // take 2 arrays (with size elements, add each item of array 1 to corresponding element of array 2 and put the result in array dst)
