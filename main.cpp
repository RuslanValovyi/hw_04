#include <iostream>
#include <cmath>
using namespace std;
#define countof(array) (sizeof(array)/sizeof(*(array)))

// return true if two given numbers are equal. use precision co control the comparison
bool equals(double a, double b, uint8_t precision=0)
{
    double epsilon = 1.0f / pow(10, precision);
	if(fabs(a - b) < epsilon)
		return true;
	return false;
} 
// take a integer and set specific bit to 0 or 1
void setBit(int& src, uint8_t bit)
{
	src |= 1<<bit;
} 
// take an integer and revert specified bit in it
void revertBit(int& src, uint8_t bit)
{
	src ^= 1<<bit;
} 
// take 2 arrays (with size elements, add each item of array 1 to corresponding element of array 2 and put the result in array dst)
bool addVector(const int* src1, const int* src2, std::size_t size1, int* dts, std::size_t size2)
{
	if (size1 > size2) {
		return false;
	}
	while (size1 > 0) {
	    *dts++= *src1++ + *src2++;
		size1--;
	}
	return true; 
}

int main (int argc, char **)
{
	double a, b;
	a = 2.12345615;
	b = 2.12345628;
	if(equals(a, b)){
	  cout << "They are equivalent" << endl;
	} else {
	  cout << "They are not equivalent" << endl;
	}
	if(equals(a, b, 6)){
	  cout << "They are equivalent" << endl;
	} else {
	  cout << "They are not equivalent" << endl;
	}
	if(equals(a, b, 7)){
	  cout << "They are equivalent" << endl;
	} else {
	  cout << "They are not equivalent" << endl;
	}
	int val = 8;
	setBit(val, 1);
	cout << "val = " << val << endl;
	revertBit(val, 1);
	cout << "val = " << val << endl;

	const int array1[] = {0x01, 0x02, 0x03, 0x04, 0x05};
	const int array2[] = {0x06, 0x07, 0x08, 0x09, 0x0A};
	int array_res[5];
	addVector(array1, array2, countof(array1), array_res, countof(array_res));
    for (int i=0; i<countof(array_res); i++) {
        cout << "array_res[" << i << "] = " << array_res[i] << endl;
    }
	return 0;
}
